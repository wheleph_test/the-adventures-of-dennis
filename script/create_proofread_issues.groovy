import groovy.io.FileType
@Grapes(
        @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')
)
import groovyx.net.http.RESTClient
import static groovyx.net.http.ContentType.*

def token = '<token>'
def projectId = '3057147'
def srcDirName = 'html_en'
def srcRelPath = "../$srcDirName"

def srcDir = new File(srcRelPath)
def gitlab = new RESTClient( 'https://gitlab.com' )

srcDir.eachFile FileType.FILES, {
    if ((it.name =~ /\d\d-.*.htm/).matches()) {
        gitlab.post(path: "/api/v4/projects/$projectId/issues",
                headers: ['PRIVATE-TOKEN': token],
                query: [
                        'title': "Proofread ${srcDirName}/${it.name}",
                        'description': "Russian text is in 'html_ru'\n\nScanned version of the book is in 'scanned_en'"
                ]
        )
    }
}


//println gitlab.get(path: "/api/v4/projects/$projectId/issues", headers: ['PRIVATE-TOKEN': token]).data