Small change

### Fetching the project

1. Install [Git](https://git-scm.com/).

2. Install [Git Large File Storage (Git LFS)](https://git-lfs.github.com/) which can be downloaded from [here](https://github.com/git-lfs/git-lfs/releases/tag/v2.0.2).

3. Do ```git clone <repo_url>```.

Sometimes files stored in Git LFS are not fetched. Check [this thread](https://github.com/git-lfs/git-lfs/issues/663) for possible solutions.

### Resources
- Snapshot of the website where the book was originally published: http://web.archive.org/web/20011106194103/http://home.freeuk.net/russica2/
- http://lingualeo.com/ru/jungle/victor-dragunsky-the-adventures-of-dennis-27827#/page/1